<!doctype html>
<html class="no-js" lang="">
<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>MMI2 EVALUATION</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="stylesheet" href="css/style.css">
	<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed|Roboto+Mono" rel="stylesheet">
</head>
<body>
<div class="layout remodal-bg">
	<header role="banner">
		<div class="wrapper">
			<p class="logo">Chuck Norris</p>
		</div>
	</header>
	<main>
		<div class="wrapper">
			<div id="pages">
				<article>
					<div class="facts">
						<p>Chargement en cours...</p>
					</div>
					<div>
						<a href="http://google.fr" class="btn">Recharger <span>X</span> <strong>Chuck Norris Fact</strong> aléatoirement</a>
					</div>
					<hr>
					<p>
						<strong>Expliquez en quelques mots la methode jQuery <i>next()</i></strong> :
						<!-- Votre réponse ici -->
					</p>
				</article>
			</div>
		</div>
	</main>
</div>

<script src="js/all.js"></script>

</body>
</html>