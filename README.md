#EVALUATION MMI2 : mise en application jQuery
> **Avant de commencer :**
> - Vous disposez d'une heure et demie pour effectuer les consignes de l'évaluation.
> - Une fois terminé, appelez votre enseignant pour la correction de votre travail.
> - Vous trouverez sous ces consignes des méthodes jQuery qui pourront vous aider.
> - Aidez-vous de la [documentation jQuery](http://api.jquery.com/){:target="_blank"} !
> - Usez et abusez des console.log() pour vous aider.
> - Bon courage !

----------

### **Consignes de l'évaluation** :

- Remplacez l'appel de **jQuery via CDN** par un jQuery **local** et placez cet appel de manière optimale dans votre page.
- Effectuez l'ensemble des consignes suivantes dans un fichier **app.js**. Celui-ci doit exécuter du code javascript uniquement lorsque le DOM est prêt.
- Créer une variable nommée **numberOfFacts** de valeur **5** (attention : cette variable ne doit pas être de type **string**, mais bien de type **int**).
- Le bouton présent dans le document contient le caractère **x** dans une balise ```<span>```. Remplacer celui-ci à l'aide de jQuery par la valeur de la variable **numberOfFacts**.
- Lors d'un click sur ce bouton, prévenir le comportement par default du navigateur à l'aide de **event.preventDefault()**.
- L'évènement click doit ensuite vider le **contenu** de l'élément de classe **.facts** du document à l'aide de l'une des méthodes suivantes : **html()**, **remove()**, **empty()**.
-  L'évènement click doit ensuite exécuter la fonction **loadFact** que vous devez créer. Cette fonction utilise un paramètre **nb** égale à **numberOfFacts**.
- Dans cette fonction, créez une boucle : **for(i = 0; i < nb; ++i)**. Dans cette boucle, utiliser la méthode **$.get()** avec l'url **"http://api.icndb.com/jokes/random"**. A l'aide de **console.log(data)** vous pourrez observer un retour de type **object** similaire à celui-ci :
```json
{ 
    "type": "success", 
    "value": { 
        "id": 3, 
        "joke": "Chuck Norris doesn't read books. He stares them down until he gets the information he wants.", 
        "categories": []
     }
 }
```
- Pour utiliser l'une des valeurs de ce retour, la valeur de **joke** par exemple, testez : **console.log(data.value.joke)**.
- Par vos observations, vous serez en mesure d'insérer la valeur de "joke" à l'intérieur de la classe **.facts** présente dans le document. Chacune d'elle doit être insérée dans une balise ```<p>```.
- Ajoutez au même moment, la valeur de l'**id** dans une balise ```<span>``` à la fin de ce paragraphe (```<p>```).
- Une question **BONUS** est présente à la fin de la liste de définitions. Répondez directement à la suite dans **index.php**.

### **Méthodes jQuery** :

**append()** // **prepend()** // **html()** // **text()** // **empty()** // **remove()** // **$.get('url',function(){ ... })** // **click(function(){ ... })**

